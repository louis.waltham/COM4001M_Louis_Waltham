import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


public class MainWindow {

	public JFrame frame;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	
	Basket basket = new Basket();
	JButton btnViewBasket = new JButton("View Basket");
	
	data dat = new data();
	MainWindow global;
	public void initialize() {
		System.out.println("Initializing.");
		global = this;
		if(dat.items.isEmpty())
		{
			//Generate the cache of default items.
			dat.cache();
		}
		frame = new JFrame();
		frame.setBounds(100, 100, 678, 502);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel SelfCheckoutTitle = new JLabel("SELF CHECKOUT");
		SelfCheckoutTitle.setForeground(new Color(0, 0, 0));
		SelfCheckoutTitle.setFont(new Font("Russo One", Font.PLAIN, 26));
		SelfCheckoutTitle.setBounds(224, 11, 263, 50);
		frame.getContentPane().add(SelfCheckoutTitle);
		
		JLabel ProductListTitle = new JLabel("Code");
		ProductListTitle.setBounds(121, 72, 31, 14);
		frame.getContentPane().add(ProductListTitle);
		
		int id = 1;
		int x = 121;
		int y = 97;
		int y_offset = 25;
		
		//Parse stock items.
		for(data.Item current : dat.items)
		{
			System.out.println("Item: " + current.name + ", Quantity:" + current.quantity);
			String firstnumber = "" + id;
			if(firstnumber.length() == 1)
			{
				firstnumber = "0" + id;
			}
			JLabel NumberLabel = new JLabel(firstnumber);
			NumberLabel.setBounds(x, y, 90, 14);
			JLabel NameLabel = new JLabel(current.name);
			NameLabel.setBounds(x + 112, y, 90, 14);
			JLabel PriceLabel = new JLabel("" + current.price);
			PriceLabel.setBounds(x + 112 + 130, y, 90, 14);
			JLabel QuantityLabel = new JLabel("" + current.quantity);
			QuantityLabel.setBounds(x + 112 + 130 + 130, y, 90, 14);
			frame.getContentPane().add(NumberLabel);
			frame.getContentPane().add(NameLabel);
			frame.getContentPane().add(PriceLabel);
			frame.getContentPane().add(QuantityLabel);
			
			id++;
			y += y_offset;
		}
		
		@SuppressWarnings("rawtypes")
		
		JComboBox DropDownBoxProducts = new JComboBox();
		DropDownBoxProducts.setToolTipText("Select Product");
		DropDownBoxProducts.addItem("Select Product");
		for(data.Item current : dat.items)
		{
			DropDownBoxProducts.addItem(current.name);
		}
		DropDownBoxProducts.setSelectedItem("null");
		
		DropDownBoxProducts.setBounds(150, 353, 173, 31);
		frame.getContentPane().add(DropDownBoxProducts);
		
		JLabel ProductListTitle_1 = new JLabel("Code");
		ProductListTitle_1.setBounds(121, 72, 31, 14);
		frame.getContentPane().add(ProductListTitle_1);
		
		JLabel lblProduct = new JLabel("Product");
		lblProduct.setBounds(234, 72, 89, 14);
		frame.getContentPane().add(lblProduct);
		
		JLabel lblPrice = new JLabel("Price(\u00A3)");
		lblPrice.setBounds(362, 72, 56, 14);
		frame.getContentPane().add(lblPrice);
		
		JLabel lblQuantity = new JLabel("Quantity");
		lblQuantity.setBounds(490, 72, 70, 14);
		frame.getContentPane().add(lblQuantity);
		
		JButton btnAddToBasket = new JButton("Add to basket");
		btnAddToBasket.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				dat.addbasket(DropDownBoxProducts.getSelectedItem().toString());
				
			}
		});
		btnAddToBasket.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnAddToBasket.setBounds(376, 357, 125, 23);
		frame.getContentPane().add(btnAddToBasket);
		
		basket = new Basket();
		btnViewBasket.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				basket.load(dat, global);
				basket.setVisible(true);
				   basket.addComponentListener ( new ComponentAdapter ()
				    {
				        public void componentHidden ( ComponentEvent e )
				        {
							System.out.println("Should reload now.");
				        	//frame.removeAll();
				            //initialize();
				        }
				    } );
			}
		});
		btnViewBasket.setBounds(563, 11, 89, 23);
		frame.getContentPane().add(btnViewBasket);
		
		textField = new JTextField();
		textField.setBounds(136, 395, 375, 31);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		
        frame.revalidate();
        frame.repaint();
	}
	
}
