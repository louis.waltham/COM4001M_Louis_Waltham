
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import java.awt.Color;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.JTable;
import javax.swing.JTextField;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.beans.PropertyChangeEvent;

public class Basket extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Basket frame = new Basket();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	public Basket() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 512, 578);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblBasket = new JLabel("Basket");
		lblBasket.setForeground(Color.BLACK);
		lblBasket.setFont(new Font("Russo One", Font.PLAIN, 26));
		lblBasket.setBounds(193, 11, 105, 50);
		contentPane.add(lblBasket);
		
		JLabel ProductListTitle = new JLabel("Code");
		ProductListTitle.setBounds(50, 76, 31, 14);
		contentPane.add(ProductListTitle);
		
		JLabel lblProduct = new JLabel("Product");
		lblProduct.setBounds(165, 76, 60, 14);
		contentPane.add(lblProduct);
		
		JLabel lblCost = new JLabel("Quantity");
		lblCost.setBounds(289, 76, 60, 14);
		contentPane.add(lblCost);
		
		JLabel lblCost_1 = new JLabel("Cost");
		lblCost_1.setBounds(436, 76, 50, 14);
		contentPane.add(lblCost_1);
	}
	
	double total_value = 0;
	MainWindow main;
	JButton payButton = new JButton("Pay");
	JSpinner AmountEntered = new JSpinner();
	public void load(data dat, MainWindow main)
	{
		this.main = main;
		int id = 1;
		int x = 60;
		int y = 97;
		int y_offset = 25;
		
		//Parse current basket.
		for(data.Item current : dat.items)
		{
			if(current.basket_quantity > 0)
			{
				String firstnumber = "" + id;
				if(firstnumber.length() == 1)
				{
					firstnumber = "0" + id;
				}
				JLabel NumberLabel = new JLabel(firstnumber);
				NumberLabel.setBounds(x, y, 90, 14);
				JLabel NameLabel = new JLabel(current.name);
				NameLabel.setBounds(x + 112, y, 90, 14);
				JLabel QuantityLabel = new JLabel("" + current.basket_quantity);
				QuantityLabel.setBounds(x + 112 + 130, y, 90, 14);
				JLabel PriceLabel = new JLabel("" + (current.basket_quantity * current.price));
				total_value += (current.basket_quantity * current.price);
				PriceLabel.setBounds(x + 112 + 130 + 130, y, 90, 14);
				contentPane.add(NumberLabel);
				contentPane.add(NameLabel);
				contentPane.add(QuantityLabel);
				contentPane.add(PriceLabel);
				
				id++;
				y += y_offset;
			}
		}
		
		y += 25;
		JLabel TotalPrice = new JLabel("Total: " + "�" + total_value);
		TotalPrice.setBounds(x, y, 90, 14);
		contentPane.add(TotalPrice);
		
		Random rand = new Random();
		int n = rand.nextInt(10) + 1;
		System.out.println("DiscountRaffled: " + n);
		
		//Calculate random discount.
		double discount_percentage = 0;
		int discount_price = 0;
		switch(n)
		{
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			discount_percentage = (n * 10);
			break;
		case 6:
		case 7:
		case 8:
		case 9:
		case 10:
			discount_price = 5;
			break;
		}
		
		double price_after_discount = 0;
		
		if(discount_percentage > 0)
		{
			y += 25;
			JLabel DiscountPercentage = new JLabel("Discount: " + discount_percentage + "%");
			DiscountPercentage.setBounds(x, y, 500, 14);
			contentPane.add(DiscountPercentage);
			double discount_calc = (100 - discount_percentage) / 100;
			price_after_discount = total_value * discount_calc;
			if(price_after_discount < 0)
			{
				price_after_discount = -1;
			}
		}
		
		if(discount_price > 0)
		{
			y += 25;
			JLabel DiscountPercentage = new JLabel("Discount: " + "�" +discount_price);
			DiscountPercentage.setBounds(x, y, 500, 14);
			contentPane.add(DiscountPercentage);
			price_after_discount = total_value - discount_price;
			if(price_after_discount < 0)
			{
				price_after_discount = -1;
			}
		}
		
		//Show discount if availible.
		if(price_after_discount > 0 || price_after_discount == -1)
		{
			if(price_after_discount < 0)
			{
				price_after_discount = 0;
			}
			y += 25;
			price_after_discount = (price_after_discount * 100 / 100);
			String discount_price_string = "" + price_after_discount;
			JLabel AfterDiscountLabel = new JLabel("Total Price after discount: " + "�" + discount_price_string);
			AfterDiscountLabel.setBounds(x, y, 500, 14);
			contentPane.add(AfterDiscountLabel);
			
			
			total_value = price_after_discount;
		}
		
		if(total_value < 0)
		{
			total_value = 0;
		}
		
		
		//Add payment option.
		y += 25;
		JLabel PaymentAmount = new JLabel("Payment amount: ");
		PaymentAmount.setBounds(x, y, 250, 14);
		contentPane.add(PaymentAmount);
		
		AmountEntered.setBounds(x + 120, y, 100, 14);
		contentPane.add(AmountEntered);
		
		y += 25;
		payButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Detect insufficeint funds.
			    if((Integer)AmountEntered.getValue() < total_value)
			    {
			    	double shortvalue = total_value - (Integer)AmountEntered.getValue();
					JOptionPane.showMessageDialog(null, "Insufficient amount, you're �" + ((shortvalue * 100) / 100) + " short.");
			    }
			    else
			    {
			    	//Generate receipt.
					JOptionPane.showMessageDialog(null, "Payment sucessfull");
					File receipt_file = new File("receipt.txt");
					try {
						receipt_file.createNewFile();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					PrintWriter writer;
					try {
						writer = new PrintWriter("receipt.txt", "UTF-8");
						writer.println("RECEIPT");
						writer.println("--------");
						for(data.Item current : dat.items)
						{
							if(current.basket_quantity > 0)
							{
								writer.println(current.name + " " + current.basket_quantity + "x");
							}
						}
						writer.println("--------");
						writer.println("Total: �" + total_value);
						writer.println("Paid: �" + (Integer)AmountEntered.getValue());
						writer.close();
						for(data.Item current : dat.items)
						{
							if(current.basket_quantity > 0)
							{
								System.out.println("Removing quantity: " + current.basket_quantity + ", From:" + current.name);
								current.quantity -= current.basket_quantity;
								current.basket_quantity = 0;
								
								System.out.println("New quantity: " + current.quantity);
							}
						}
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					setVisible(false);
					main.frame.setVisible(false);
					main.initialize();
					main.frame.repaint();
					main.frame.revalidate();
					main.frame.setVisible(true);
			    }
			}
		});
		payButton.setBounds(x, y, 100, 14);
		contentPane.add(payButton);
		
	}
}
