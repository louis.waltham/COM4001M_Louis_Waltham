import java.util.ArrayList;

import javax.swing.JOptionPane;

public class data {
	// variables 
	public class Item
	{
		String name;
		Integer quantity;
		Integer basket_quantity;
		Integer price;
		
		public Item(String name, Integer quantity, Integer price)
		{
			this.name = name;
			this.quantity = quantity;
			this.price = price;
			this.basket_quantity = 0;
		}
	}
	
	public ArrayList<Item> items = new ArrayList<Item>();
	
	// item data list
	public void cache()
	{
		items.add(new Item("Pen", 20, 1));
		items.add(new Item("Book", 20, 1));
		items.add(new Item("Water", 20, 1));
		items.add(new Item("Drink", 20, 1));
		items.add(new Item("Snack", 20, 2));
		items.add(new Item("Sweet", 20, 1));
		items.add(new Item("Chocolate", 20, 1));
		items.add(new Item("Biscuits", 20, 1));
		items.add(new Item("Cake", 20, 2));

		
	}
	
	public Item getItemByName(String name)
	{
		for(Item i : items)
		{
			if(i.name.equals(name))
			{
				return i;
			}
		}
		
		return null;
	}
	
	public Item getBasketItemByName(String name)
	{
		for(Item i : items)
		{
			if(i.name.equals(name))
			{
				return i;
			}
		}
		
		return null;
	}
	
	public void addbasket(String name)
	{
		for(Item i : items)
		{
			if(i.name.equals(name))
			{
				System.out.println("Quantity: " + i.quantity + ", Basket:" + i.basket_quantity);
				if(i.quantity >= (i.basket_quantity + 1))
				{
					i.basket_quantity++;
				}
				else
				{
					//MESSAGEBOX
					JOptionPane.showMessageDialog(null, name + " is out of stock.");
				}
				return;
			}
		}
		
		
	}
}
